# Reto Aspirantes DevSecOps

Materiales:

-   Aplicación NodeJS ./app
-   URL: <https://gitlab.com/tests-for-applicants-dso-inm/reto-devops> 

---------------------------------------------------------------------------------------------------------------------------------------------

Levantar aplicación NodeJS con Docker Compose

Pasos:

-   Crear imagen docker: Se debe ejecutar la siguiente sentencia en raíz del proyecto:
    ```
    docker build -t test-inmetrics:v1.0.0 .
    ```
-   Luego de tener la imagen creada, levantar servicio ejecutando la siguiente sentencia en raíz del proyecto:
    ```
    docker-compose up -d test-inmetrics
    ```

    Nota: El servicio quedará expuesto en el puerto 3000 de la máquina host.

---------------------------------------------------------------------------------------------------------------------------------------------

Crear pipeline para gitlab-ci

Pasos:

-   Levantar Runner local: Para la ejecución de la prueba se levanta un runner local, el cual se encuentra declarado en el mismo docker-compose.yml del servicio NodeJs. Para levantar este servicio se debe ejecutar la siguiente sentencia:
    ```
    docker-compose up -d gitlab-runner-2
    ```
-   Registrar App en Runner local: Para que el runner opere se debe registrar la aplicación de la siguiente manera:
    ```
    docker-compose exec gitlab-runner-2 \
    gitlab-runner register \
    --non-interactive \
    --url yourURlGitlab \
    --registration-token yourRegistrationToken \
    --executor docker \
    --description "Sample Runner 1" \
    --docker-image "docker:stable" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock
    ```

    Nota: los campos "yourUrlGitlab" y "yourRegistrationToken" se obtienen desde gitlab:
    ![image.png](./image.png)


-    Configurar .gitlab-ci.yml
    Se deben setear en archivo los parámetros:
    
    1. VERSION: versión de la imagen
    2. CONTAINER_RELEASE:IMAGE: nombre de imagen
    3. USER _REGISTRY: Usuario en registry
    4. PERSONAL_TOKEN_REGISTRY: token personal de usuario en registry
    5. URL_REGISTRY: url de registry
    

    test-dani-1
