FROM node:16-alpine

RUN apk add --update alpine-sdk

ENV APP_HOME /opt/reto-devops/
COPY . $APP_HOME/
WORKDIR $APP_HOME/app


RUN npm install

COPY . .


EXPOSE 3000

CMD [ "npm", "start"]
